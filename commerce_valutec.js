function getGift() {
	
  $('#gift-discount-message').remove();
	
  var code = $('#edit-panes-gift-discount-code');
	var ordertotal=$('#edit-panes-payment-current-total').val();
	
  $.ajax({
    type: "POST",
    url: Drupal.settings['base_path'] + "?q=cart/checkout/gift_discount",
    data: {
      code: code.val(),
	  total: ordertotal
    },
    dataType: "json",
    success: function(gift_discount) {
      code.parent().next().after('<div id="gift-discount-message">' + gift_discount.message + '</div>');
		$('#edit-panes-gift-discount-apply').attr("disabled", false);
      if (gift_discount.valid) {
        if (window.set_line_item) {
          set_line_item('gift_discount', gift_discount.title, -gift_discount.amount, 2);
		  var temptotal=parseFloat(ordertotal)-parseFloat(gift_discount.amount);
		  $('#edit-panes-gift-discount-mytotal').val(temptotal);
        }
      }
      else {
        if (window.remove_line_item) {
          remove_line_item('gift_discount');
        }
      }

      if (window.getTax) {
        getTax();
      }
      else if (window.render_line_items) {
        render_line_items();
      }
    }
  });
  $('#edit-panes-gift-discount-temp').val(ordertotal);
  
  
}

$(function() {
  if (Drupal.settings.uc_gift_discount && window.set_line_item) {
    set_line_item('gift_discount', Drupal.settings.uc_gift_discount.title, -Drupal.settings.uc_gift_discount.amount, 2);
  }
});


function check_balance() {
	
  $('#gift-discount-message').remove();
	$('#edit-panes-gift-discount-apply').attr("disabled", true);
  	var code = $('#edit-panes-gift-discount-code');
	var ordertotal=$('#edit-panes-payment-current-total').val();
  $.ajax({
    type: "POST",
    url: Drupal.settings['base_path'] + "?q=cart/checkout/gift_discount",
    data: {
      code: code.val(),
	  method: 'checkbalance',
	  total: ordertotal
    },
    dataType: "json",
    success: function(gift_discount) {
      code.parent().next().after('<div id="gift-discount-message">' + gift_discount.message + '</div>');
	  if(gift_discount.error=='True')
	  	$('#edit-panes-gift-discount-apply').attr("disabled", false);
    }
  });
}

function remove() {
  $('#gift-discount-message').remove();
  $('#edit-panes-gift-discount-apply').attr("disabled", false);
}

