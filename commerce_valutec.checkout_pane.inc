<?php

/**
 * @file
 * commerce_valutec checkout panes.
 *
 * Expose a textfield for entering a gift card number during checkout, and show list
 * of gift cards applied to current order.
 */

/**
 * Payment pane: form callback.
 */
function commerce_valutec_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $pane_form = array(
    '#prefix' => '<div id="commerce-checkout-valutec-gift-card-wrapper">',
    '#suffix' => '</div>',
  );

  // Store the payment methods in the form for validation purposes.
  $pane_form['commerce_valutec_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Gift Card Number'),
    '#description' => t('Enter here your gift card number.'),
  );
  if (!empty($order->data['gift_card'] )&& $order->data['gift_card']['is_valid']) {
    $pane_form['commerce_valutec_code']['#default_value'] = $order->data['gift_card']['number'];
  }

  // Display any new status messages added by this pane within the pane's area.
  if (drupal_get_messages(NULL, FALSE)) {
    $pane_form['status_messages'] = array(
      '#type' => 'markup',
      '#markup' => theme('status_messages'),
      '#weight' => -1,
    );
  }

  return $pane_form;
}


/**
 * Check whether the gift card number is valid.
 */
function commerce_valutec_pane_checkout_form_validate($form, &$form_state, $checkout_pane, $order) {
  if (isset($form['#id']) && $form['#id'] == 'edit-payment-details') {
    $gift_card_number = $form_state['commerce_valutec_code'];
    $pane = 'payment';
  } else {
    $gift_card_number = $form_state['values']['commerce_valutec']['commerce_valutec_code'];
    $pane = 'checkout';
  }
  // Check if gift card number is empty.
  if (empty($gift_card_number)) {
    return $pane == 'checkout' ? TRUE : FALSE; // On checkout it could be empty - means we are not going to use it.
  }
  // Validate the gift card number.
  if ($gift_card = commerce_valutec_card_number_is_valid($gift_card_number, $order)) {
    if (is_object($gift_card) && $gift_card->is_valid && $gift_card->balance > 0.00){
      $order->data['gift_card'] = (array)$gift_card;
      return TRUE;
    }
    else {
      if (is_string($gift_card)) {
        // we got an error message instead of Gift Card object:
        $message = $gift_card;
      }
      elseif (!$gift_card->is_valid) {
        $message = t('The Gift Card number is not valid.');
      }
      else {
        $message = t('The Gift Card balance is $!balance', array('!balance' => $gift_card->balance));
      }
    }
  }
  else {
    $message = t('Invalid Gift Card');
  }
  $pane == 'payment' ?  form_set_error('commerce_valutec_code', $message) : form_set_error('commerce_valutec][commerce_valutec_code', $message);
  return FALSE;
}

/**
 * We have a valid gift card number, so submit it.
 */
function commerce_valutec_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  // Only attach gift card to the order if no validation errors on form and gift card number is set
  // This prevents trying to add the same gift card twice on validation error in another pane
  if (!form_get_errors() && $number = $form_state['values']['commerce_valutec']['commerce_valutec_code']) {
    $commerce_valutec = commerce_valutec_load_by_number($number);
    if (is_object($commerce_valutec)) {
      $order->data['gift_card'] = (array)$commerce_valutec;
    }
    else {
      unset($order->data['gift_card']);
    }

    // Rebuild the form only if "Add gift card" ajax submit was used
    // and we're still staying on the same page.
    if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'commerce_valutec_add') {
      $form_state['rebuild'] = TRUE;
    }
  }
}


/**
 * Implements the callback for the checkout pane review form
 */
function commerce_valutec_pane_review($form, $form_state, $checkout_pane, $order) {
  if (!empty($order->data['gift_card'] )&& $order->data['gift_card']['is_valid']) {
    return t('Gift Card Number: @number', array('@number' => $order->data['gift_card']['number']));
  }
}

/**
 * Checkout pane callback: returns the cart contents pane's settings form.
 */
function commerce_valutec_pane_settings_form($checkout_pane) {
  $form = array();

  // Build an options array of Views available for the cart contents pane.
  $options = array();

  // Generate an option list from all user defined and module defined views.
  foreach (views_get_all_views() as $view_id => $view_value) {
    // Only include line item Views.
    if ($view_value->base_table == 'commerce_valutec') {
      foreach ($view_value->display as $display_id => $display_value) {
        $options[check_plain($view_id)][$view_id . '|' . $display_id] = check_plain($display_value->display_title);
      }
    }
  }

  $form['commerce_valutec_checkout_pane_view'] = array(
    '#type' => 'select',
    '#title' => t('Gift Cards Checkout Pane View'),
    '#description' => t('Specify the View to use in the checkout pane under the gift card form. Select \'none\' to not display anything.'),
    '#options' => array('none' => t('None')) + $options,
    '#default_value' => variable_get('commerce_valutec_checkout_pane_view', 'commerce_valutec_review_pane|checkout'),
  );

  $form['commerce_valutec_review_pane_view'] = array(
    '#type' => 'select',
    '#title' => t('Gift Cards Review Pane View'),
    '#description' => t('Specify the View to use in the review pane to display the gift cards.'),
    '#options' => $options,
    '#default_value' => variable_get('commerce_valutec_review_pane_view', 'commerce_valutec_review_pane|review'),
  );

  $form['commerce_valutec_checkout_enable_add_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable <em>Add gift card</em> button'),
    '#description' => t('Enabling <em>Add gift card</em> button provides option to add multiple gift card codes from <em>Gift Cards</em> checkout pane.'),
    '#default_value' => variable_get('commerce_valutec_checkout_enable_add_button', TRUE),
  );

  return $form;
}

function commerce_valutec_form_commerce_payment_order_transaction_add_form_alter(&$form, &$form_state) {
  if (isset($form['payment_terminal']['payment_details']['commerce_valutec_code'])) {
    $form['payment_terminal']['payment_details']['commerce_valutec_code']['#required'] = TRUE;
  }
}