<?php

/**
 * @file
 * Valutec Gift card rules integration file.
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_valutec_rules_event_info() {
  $events = array();

  $events['commerce_valutec_validate'] = array(
    'label' => t('Validate a gift card'),
    'group' => t('Valutec Gift Card'),
    'variables' => array(
      'commerce_valutec' => array(
        'type' => 'commerce_valutec',
        'label' => t('Valutec gift card to validate'),
      ),
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_valutec_redeem'] = array(
    'label' => t('Redeem a gift card'),
    'group' => t('Valutec Gift card'),
    'variables' => array(
      'commerce_valutec' => array(
        'type' => 'commerce_valutec',
        'label' => t('Valutec gift card to redeem'),
      ),
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}

function commerce_valutec_action_is_valid_gift_card($override = FALSE) {
  $validation_results = &drupal_static('commerce_valutec_action_validation_results');

  if (isset($validation_results) && $override == FALSE) {
    $validation_results = $validation_results && TRUE;
  }
  else {
    $validation_results = TRUE;
  }
}

function commerce_valutec_action_is_invalid_gift_card() {
  $validation_results = &drupal_static('commerce_valutec_action_validation_results');
  $validation_results = FALSE;
}
