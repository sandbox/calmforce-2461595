<?php
/**
 * @file
 * Provides Valutec Gift Card Discount for Drupal Commerce.
 *
 * Original code by Vladimir Ivanov (vladimir@calmforce.com)
 *
 * Send any suggestions and feedback to the above address.
 */
module_load_include('inc', 'commerce_valutec', 'commerce_valutec.rules');
module_load_include('inc', 'commerce_valutec', 'commerce_valutec.checkout_pane');

/**
 * Implements hook_entity_info().
 */
function commerce_valutec_entity_info() {
  return array(
    'commerce_valutec' => array(
      'label' => t('Valutec Gift Card transaction'),
      'entity class' => 'Entity',
      'controller class' => 'DrupalCommerceEntityController',
      'base table' => 'commerce_valutec_txn',
      'module' => 'commerce_valutec',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'txn_id',
      ),
      // Make use the class' label() and uri() implementation by default.
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
    ),
  );
}

function commerce_valutec_entity_property_info() {
  $info = array();

  // Add meta-data about the basic commerce_valutec properties.
  $properties = &$info['commerce_valutec']['properties'];
  $properties['txn_id'] = array(
    'type' => 'integer',
    'label' => t('Gift Card Transaction ID', array(), array('context' => 'a drupal commerce valutec gift card')),
    'description' => t('The internal numeric ID of the transaction.'),
    'schema field' => 'txn_id',
  );
  $properties['is_valid'] = array(
    'type' => 'boolean',
    'label' => t('Is valid'),
    'description' => t('Boolean indicating whether the gift card is valid.'),
    'getter callback' => 'commerce_valutec_get_properties',
  );
  $properties['balance'] = array(
    'type' => 'decimal',
    'label' => t('Gift Card Balance'),
    'description' => t('Available balance on the card.'),
    'getter callback' => 'commerce_valutec_get_properties',
  );
  $properties['price_component_name'] = array(
    'label' => t('Price component name'),
    'description' => t('Identifies the component name for each gift card'),
    'type' => 'token',
    'getter callback' => 'commerce_valutec_get_properties',
  );
  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the gift card transaction was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'created',
  );
  return $info;
}

/**
 * Callback for getting gift_card properties.
 * @see commerce_valutec_entity_property_info()
 */
function commerce_valutec_get_properties($gift_card, array $options, $name) {
  switch ($name) {
    case 'price_component_name':
      return 'commerce_valutec';
    case 'is_valid':
      return $gift_card->is_valid;
    case 'balance':
      return $gift_card->balance;
  }
  return NULL;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_valutec_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['valutec_gift_card'] = array(
    'base' => 'commerce_valutec_payment',
    'title' => t('Valutec Gift Card'),
    'short_title' => t('Valutec GC'),
    'display_title' => t('Gift card'),
    'description' => t('Implements Valutec Gift Card payment method during checkout.'),
    'active' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: submit form.
 */
function commerce_valutec_payment_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  // Allow to replace pane content with ajax calls.
  $pane_form = array(
    '#prefix' => '<div id="commerce-checkout-valutec-gift-card-ajax-wrapper">',
    '#suffix' => '</div>',
  );

  // Store the payment methods in the form for validation purposes.
  $pane_form['commerce_valutec_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Gift Card Number'),
    '#description' => t('Enter here your gift card number.'),
  );
  return $pane_form;
}

/**
 * Payment method callback: submit form validation.
 */
function commerce_valutec_payment_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  return commerce_valutec_pane_checkout_form_validate($pane_form, $pane_values, $pane_form, $order);
}

/**
 * Payment method callback: submit form submission.
 */
function commerce_valutec_payment_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order->data['gift_card']['number'] = $pane_values['commerce_valutec_code'];
  // The validation passed since we are here:
  $order->data['gift_card']['is_valid'] = TRUE;

  $transaction = commerce_valutec_payment_transaction($payment_method, $order, $charge);
  if ($transaction && $transaction->status == COMMERCE_PAYMENT_STATUS_SUCCESS) {
    return TRUE;
  }
  else {
    drupal_set_message(t('Failed to create Gift Card payment transaction'), 'error');
    return FALSE;
  }
}

/**
 * Creates a payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 */
function commerce_valutec_payment_transaction($payment_method, $order, $charge) {


  if (empty($order->data['gift_card']) || !$order->data['gift_card']['is_valid'] || !$order->data['gift_card']['balance']) {
    return NULL;
  }

  $card_balance = $order->data['gift_card']['balance'] * 100; // converting to the Commerce standard - cents
  if ($card_balance < $charge['amount']) {
    $charge['amount'] = $card_balance;
    drupal_set_message(t('The Gift Card balance was $@balance, less than required transaction amount', array('@balance' => $order->data['gift_card']['balance'])));
  }
  $transaction = commerce_payment_transaction_new('valutec_gift_card', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];

  $chargeGiftCardResult = ValutecGiftCardService::chargeGiftCard($order->data['gift_card']['number'], $charge['amount']/100.);
  if ($chargeGiftCardResult['authorized']) {
    $transaction->remote_id = $chargeGiftCardResult['txn_id'];
    $transaction->status = $transaction->remote_status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->message = 'Valutec Gift Card payment successful, transaction ID: @id';
    $transaction->message_variables = array('@id' => $transaction->remote_id);
  }
  else {
    $transaction->status = $transaction->remote_status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $transaction->message = 'Valutec Gift Card payment failed, error: @error';
    $transaction->message_variables = array('@error' => $chargeGiftCardResult['error']);
  }
  $transaction->payload[REQUEST_TIME] = array('card_number' => $order->data['gift_card']['number'], 'card_balance' => $chargeGiftCardResult['balance']);
  unset($order->data['gift_card']);
  commerce_payment_transaction_save($transaction);
  return $transaction;
}

/**
 * Returns an initialized payment transaction object.
 *
 * @param $method_id
 *   The method_id of the payment method for the transaction.
 *
 * @return
 *   A transaction object with all default fields initialized.
 */
function commerce_valutec_payment_transaction_new($gift_card, $order_id = 0) {
  global $user;
  return entity_get_controller('commerce_valutec')->create(array(
    'uid' => $user->uid,
    'card_number' => $gift_card->number,
    'is_valid' => $gift_card->is_valid,
    'balance' => $gift_card->balance,
    'order_id' => $order_id,
    'created' => REQUEST_TIME,
  ));
}

/**
 * Saves a payment transaction.
 *
 * @param $transaction
 *   The full transaction object to save.
 *
 * @return
 *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
 */
function commerce_valutec_payment_transaction_save($transaction) {
  return entity_get_controller('commerce_valutec')->save($transaction);
}

function commerce_valutec_commerce_payment_method_info_alter(&$payment_methods) {
  $payment_methods = $payment_methods;
}

function commerce_valutec_commerce_checkout_pane_info_alter(&$checkout_panes) {
  $checkout_panes = $checkout_panes;
}

/**
 * Implementation of hook_menu().
 */
function commerce_valutec_menu() {
  $items = array();

  $items['admin/commerce/config/commerce_valutec'] = array(
    'title' => 'Valutec Gift Card settings',
    'description' => 'View the Valutec Gift Card settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_valutec_settings_overview'),
    'access arguments' => array('administer Valutec gift card settings'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['cart/checkout/commerce_valutec'] = array(
    'page callback' => 'commerce_valutec_checkout_apply',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 *
 * Provides the basic access objects
 */
function commerce_valutec_permission() {
  $permissions = array(
    'administer Valutec gift card settings' => array(
      'title' => t('Administer Valutec Gift Card Settings'),
      'description' => t('Allows users to manage Valutec gift card settings.'),
    ),
  );

  return $permissions;
}


/**
 * Implements hook_commerce_checkout_pane_info().
 */
function commerce_valutec_commerce_checkout_pane_info() {

  // TODO: use variable_get('commerce_valutec_checkout', FALSE)

  $checkout_panes['commerce_valutec'] = array(
    'title' => t('Valutec Gift Card'),
    'page' => 'checkout',
    'locked' => FALSE,
    'file' => 'commerce_valutec.checkout_pane.inc',
    'base' => 'commerce_valutec_pane',
    'weight' => 10,
  );

  return $checkout_panes;
}

/**
 * Form builder for the admin settings.
 */

function commerce_valutec_settings_overview() {

 $form['commerce_valutec_checkout'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Valutec Gift Card as payment method'),
    '#summary callback' => 'summarize_checkbox',
    '#default_value' => variable_get('commerce_valutec_checkout', FALSE),
  );

  $form['commerce_valutec_soap_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Valutec SOAP WSDL URL'),
    '#default_value' => variable_get('commerce_valutec_soap_url', 'http://ws.valutec.net/Valutec.asmx?WSDL'),
    '#required' => TRUE,
  );

  $form['commerce_valutec_client_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Key'),
    '#default_value' => variable_get('commerce_valutec_client_key', ''),
    '#required' => TRUE,
  );
  
  $form['commerce_valutec_terminal_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Terminal ID'),
    '#default_value' => variable_get('commerce_valutec_terminal_id', ''),
    '#required' => TRUE,
  );
  
  $form['commerce_valutec_program_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Program Type'),
    '#default_value' => variable_get('commerce_valutec_program_type', 'Gift'),
    '#required' => TRUE,
  );
  
  $form['commerce_valutec_identifier'] = array(
    '#type' => 'textfield',
    '#title' => t('Identifier'),
    '#default_value' => variable_get('commerce_valutec_identifier', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Validates the Valutec Gift Card settings form.
 */

function commerce_valutec_settings_overview_validate($form, &$form_state) {
  if ($form_state['values']['commerce_valutec_checkout'] == TRUE) {
    if (!$form_state['values']['commerce_valutec_terminal_id']) {
      form_set_error('commerce_valutec_terminal_id', t("Terminal ID MUST not be 0"));
    }
    if (!$form_state['values']['commerce_valutec_identifier']) {
      form_set_error('commerce_valutec_identifier', t("Identifier MUST not be 0"));
    }
  }
}

function _commerce_valutec_date_range($form_element) {
  $form_element['year']['#options'] = drupal_map_assoc(range(2008, 2020));
  return $form_element;
}

/**
 * Checks if a given gift card is valid for a given order. The validation is done
 * by the rules engine.
 *
 * @TODO: We can throw exception with error message, instead of setting the error
 *  message to a global variable.
 *
 * @param $number
 *   The gift card number to validate.
 * @param $order
 *   The order at against the $number should be validated.
 * @return boolean
 *   Returns TRUE if the number is valid else FALSE.
 */
function commerce_valutec_card_number_is_valid($number, $order) {
  // Trim trailing spaces
  $number = trim($number);
  $gift_card = commerce_valutec_load_by_number($number);

  // if no such gift_card found, the $number is invalid
  if (!is_object($gift_card)) {
    return is_string($gift_card) ? $gift_card : t('Gift Card number is invalid.');
  }

  // We invoke the rules. The defined action callback methods sets then the
  // validation result appropriate.
  rules_invoke_event('commerce_valutec_validate', $gift_card, $order);

  // We get our global variable and return the result.
  return $gift_card;
}

function commerce_valutec_get_other_line_items_amount($order_wrapper) {
  // First determine the currency used for the order total.
  $currency_code = commerce_default_currency();
  // Then loop over each line item and add its total to the order total minus the gift card.
  $amount = 0;
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    $line_item = $line_item_wrapper->value();
    if ($line_item->type == 'commerce_valutec') {
      continue;
    }
    // Convert the line item's total to the order's currency for totalling.
    $component_total = commerce_price_component_total($line_item_wrapper->commerce_total->value());

    // Add the totals.
    $amount += commerce_currency_convert(
      $component_total['amount'],
      $component_total['currency_code'],
      $currency_code
    );
  }

  return $amount;// round($amount); VI: rounding results in the negative 1 c order total, not good.
}


/**
 * Redeem a gift_card. For calculating the gift_card value the rules engine is used.
 *
 * @param $gift_card
 *   The gift_card to redeem.
 * @param $order
 *   The order on which the gift_card should be redeemed.
 * @return void
 */
function commerce_valutec_redeem_gift_card($gift_card, $order) {
  if (!isset($order->order_id) or !isset($gift_card->balance)) {
    drupal_set_message(t('Your gift card number cannot be redeemed.'), 'error');
    return;
  }
  if ($gift_card->balance <= 1) {
    drupal_set_message(t('Your gift card has insufficient balance (less than $1).'), 'error');
    return;
  }
  // Create a payment transaction.
  $gift_card_transaction = commerce_valutec_payment_transaction_new($gift_card, $order->order_id);
  commerce_valutec_payment_transaction_save($gift_card_transaction);
  $gift_card_wrapper = entity_metadata_wrapper('commerce_valutec', $gift_card_transaction);
  // We invoke the rule. The defined action callback methods sets then the
  // gift card value appropriate.
  rules_invoke_event('commerce_valutec_redeem', $gift_card_transaction, $order);
  commerce_order_save($order);
}

/**
 * Loads a gift card by its gift card number.
 *
 * @param $number
 *   A number of a gift card.
 * @return
 *   A gift card object corresponding to the gift card number.
 */
function commerce_valutec_load_by_number($number) {

  $result = ValutecGiftCardService::validateCard($number);
  if (!$result) {
    return NULL;
  }
  elseif (is_string($result)) {
    // Error message:
    return $result;
  }
  $result = ValutecGiftCardService::getCardBalance($number);
  if (!$result['authorized']) {
    return $result['error'];
  }
  $gift_card = new stdClass();
  $gift_card->is_valid = TRUE;
  $gift_card->number = $number;
  $gift_card->balance = $result['balance'];
  return $gift_card;
}


/**
 * Implements hook_form_alter().
 */
function commerce_valutec_form_commerce_checkout_form_review_alter(&$form, &$form_state) {
  // Alter the checkout form
  // The payment method introduced by this module should not be listed as an option on the form, removing the Gift Card payment method from radio list:
  unset($form['commerce_payment']['payment_method']['#options']['valutec_gift_card|commerce_payment_valutec_gift_card']);

  if (isset($form_state['order']->data['gift_card']) && $form_state['order']->data['gift_card']['is_valid']) {

    $balance = commerce_payment_order_balance($form_state['order']);
    if ($balance['amount'] < 100 * $form_state['order']->data['gift_card']['balance']) { // the gift card amount in dollars, the order is in cents
        // removing all payment methods:
      unset($form['commerce_payment']);
      $form['checkout_review']['review']['#data']['commerce_valutec']['data'] .=
        '</br>' . t("Card balance: $!balance", array('!balance' => $form_state['order']->data['gift_card']['balance'])) .
        '</br><h4 class="gift-card-message">'. t('The Gift Card has enough funds to pay for the entire order. ') . '</h4>';

    }
    elseif($form_state['order']->data['gift_card']['balance']) {
      $form['checkout_review']['review']['#data']['commerce_valutec']['data'] .=
        '</br>' . t("Card balance: $!balance", array('!balance' => $form_state['order']->data['gift_card']['balance'])) .
        '</br><h4 class="gift-card-message">'. t('The Gift Card does not have enough funds to pay for the entire order, so please select other payment method as well.') . '</h4>';
    }
  }
}

/**
 * Implements hook_commerce_order_presave()
 *
 * @param $order
 *  The commerce_order entity just about to be saved.
 */
function commerce_valutec_commerce_order_presave($order) {

  if (in_array($order->status, array('checkout_complete', 'pending')) &&
      isset($order->data['gift_card']) && $order->data['gift_card']['is_valid']) {

    $balance = commerce_payment_order_balance($order);
    if ($balance['amount'] && $balance['amount'] < 100 * $order->data['gift_card']['balance']) {
      $payment_method = commerce_payment_method_load('valutec_gift_card');
      $payment_method['instance_id'] = 'valutec_gift_card|commerce_payment_valutec_gift_card';
      commerce_valutec_payment_transaction($payment_method, $order, $balance);
    }

    unset($order->data['gift_card']);
  }
}

function commerce_valutec_commerce_payment_transaction_presave($transaction) {

  if ($transaction->payment_method != 'valutec_gift_card' && $transaction->status != COMMERCE_PAYMENT_STATUS_FAILURE
      && $order = commerce_order_load($transaction->order_id)) {

    if (isset($order->data['gift_card']) && $order->data['gift_card']['is_valid'] && $order->status == 'checkout_review') {

      $transaction->amount -= $order->data['gift_card']['balance'] * 100;
      // create one more transaction - gift card
      $payment_method = commerce_payment_method_load('valutec_gift_card');
      $payment_method['instance_id'] = 'valutec_gift_card|commerce_payment_valutec_gift_card';
      $balance = array('amount' => $order->data['gift_card']['balance'] * 100, 'currency_code' => $transaction->currency_code);
      commerce_valutec_payment_transaction($payment_method, $order, $balance);

    }
  }

}