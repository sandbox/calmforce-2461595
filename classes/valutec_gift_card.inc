<?php

/**
 * @file
 * Valutec Gift Card class.
 */

class ValutecGiftCardService {

  private static $valutec_soap_url;
  private static $transaction_params_common;

  /**
   * Singleton instance of ValutecGiftCardService
   *
   * @var ValutecGiftCardService
   */
  protected static $instance = null;

  /**
   * Creates a gift card object.
   */
  public function __construct() {
    self::$valutec_soap_url = variable_get('commerce_valutec_soap_url', '');
    self::$transaction_params_common = array(
      'ClientKey' => variable_get('commerce_valutec_client_key', ''),
      'TerminalID' => variable_get('commerce_valutec_terminal_id', ''),
      'ProgramType' => variable_get('commerce_valutec_program_type','Gift'),
      'Identifier' => variable_get('commerce_valutec_identifier', ''),
      'ServerID' => "",
    );
    if (empty(self::$valutec_soap_url) || empty(self::$transaction_params_common['ClientKey'])) {
      throw new Exception(t('Valutec Gift Card client account has not been configured!'));
    }
    $soapClient = new SoapClient(self::$valutec_soap_url);
    if (!$soapClient) {
      throw new Exception('Unable to connect to Valutec SOAP server');
    }
    $transaction_params = self::$transaction_params_common;
    $transaction_params['TotalType'] = "CurrentDay";
    $result = $soapClient->Transaction_HostTotals($transaction_params);
    if (!$result->Transaction_HostTotalsResult->Authorized) {
      throw new Exception($result->Transaction_HostTotalsResult->ErrorMsg);
    }
  }

  /**
   * Creates ValutecGiftCardService object and stores it for singleton access
   *
   * @return ValutecGiftCardService
   */
  public static function init() {
    try {
      return self::$instance = new self();
    }
    catch (Exception $e) {
      drupal_set_message($e->getMessage(),'error');
      self::$instance = NULL;
      return NULL;
    }
  }

  /**
   * Gets singleton instance of ValutecGiftCardService
   *
   * @param boolean $AutoCreate
   * @return ValutecGiftCardService
   */
  public static function getInstance($AutoCreate = FALSE) {
    if ($AutoCreate === TRUE && !self::$instance) {
      self::init();
    }
    return self::$instance;
  }

  /**
   * Validates ValutecGiftCardService with particular CardNumber
   *
   * @param string $CardNumber
   * @return boolean TRUE or FALSE
   */
  public static function validateCard($CardNumber) {
    if (empty($CardNumber)) {
      return FALSE;
    }
    if (!self::getInstance(TRUE)) {
      return FALSE;
    }

    $soapClient = new SoapClient(self::$valutec_soap_url);
    if (!$soapClient) {
      throw new Exception('Unable to connect to Valutec SOAP server');
    }

    $transaction_params = self::$transaction_params_common;
    unset($transaction_params['Identifier']);
    unset($transaction_params['ProgramType']);
    unset($transaction_params['ServerID']);
    $transaction_params['CardNumber'] = $CardNumber;
    $result = $soapClient->Registration_Get($transaction_params);
    unset($soapClient);
    if (!$result->Registration_GetResult->Authorized) {
      $error = $result->Registration_GetResult->ErrMessage;
      drupal_set_message($result->Registration_GetResult->ErrMessage, 'error');
      return $error;
    }
    return TRUE;
  }

  /**
   * Gets card balance for particular CardNumber
   *
   * @param string $CardNumber
   * @return mixed;
   */
  public static function getCardBalance($CardNumber) {
    if (empty($CardNumber)) {
      return array('authorized' => 0, 'error' =>  'No Card Number provided');
    }
    if (!self::getInstance(TRUE)) {
      return array('authorized' => 0, 'error' => 'Unable to initialize SoapClient for ValutecGiftCardService');
    }

    $transaction_params = self::$transaction_params_common;
    $transaction_params['CardNumber'] = $CardNumber;
    $soapClient = new SoapClient(self::$valutec_soap_url);
    if (!$soapClient) {
      throw new Exception('Unable to connect to Valutec SOAP server');
    }
    $result = $soapClient->Transaction_CardBalance($transaction_params);
    $return = array('authorized' => $result->Transaction_CardBalanceResult->Authorized);
    if ($return['authorized']) {
      $return['balance'] = $result->Transaction_CardBalanceResult->Balance;
    }
    else {
      $return['error'] = $result->Transaction_CardBalanceResult->ErrorMsg;
    }
    unset($soapClient);
    return $return;
  }

  /**
   * Gets card balance for particular CardNumber
   *
   * @param string $CardNumber
   * @return mixed;
   */
  public static function chargeGiftCard($CardNumber, $Amount) {

    if (empty($CardNumber) || !$Amount) {
      return array('authorized' => 0, 'error' =>  empty($CardNumber) ? 'No Card Number provided' : 'The Amount to be charged is $0');
    }
    if (!self::getInstance(TRUE)) {
      return array('authorized' => 0, 'error' => 'Unable to initialize SoapClient for ValutecGiftCardService');
    }

    $transaction_params = self::$transaction_params_common;
    $transaction_params['CardNumber'] = $CardNumber;
    $transaction_params['Amount'] = $Amount;

    $soapClient = new SoapClient(self::$valutec_soap_url);
    if (!$soapClient) {
      throw new Exception('Unable to connect to Valutec SOAP server');
    }
    $result = $soapClient->Transaction_Sale($transaction_params);

    $return = array('authorized' => $result->Transaction_SaleResult->Authorized);
    if ($return['authorized'] == 1) {
      $return['txn_id'] = $result->Transaction_SaleResult->AuthorizationCode;
      $return['balance'] = $result->Transaction_SaleResult->Balance;
    }
    else {
      $return['error'] = $result->Transaction_SaleResult->ErrorMsg;
    }
    unset($soapClient);
    return $return;
  }

}
